<?php

namespace App\Form;

use App\Entity\Demand;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class DemandType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Title')
            ->add('Content')
            ->add('PieceJointe', FileType::class, array('data_class' => null))
            ->add('CreationDate', DateType::class, [
                'widget' => 'single_text',
                'input'  => 'datetime',
                'html5'=>'false'
            ])
            ->add('status' , ChoiceType::class, [
                'choices' => [
                    'waiting' => 'waiting',
                    'in progress' => 'in progress',
                    'closed' => 'closed',
                ],
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Demand::class,
        ]);
    }
}
