<?php

namespace App\Form;

use App\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
//ajout du use pour utiliser le type input password de Symfony
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UtilisateurType extends AbstractType
{
        public function buildForm(FormBuilderInterface $builder, array $options)
        {
                $builder
                        ->add('username')
                        ->add('roles', ChoiceType::class, array(
                                'attr'  =>  array('class' => 'form-control',
                                'style' => 'margin:5px 0;'),
                                'choices' => 
                                array
                                (
                                    'ROLE_ADMIN' => array
                                    (
                                        'Yes' => 'ROLE_ADMIN',
                                    ),
                                    'ROLE_USER' => array
                                    (
                                        'Yes' => 'ROLE_USER'
                                    ),
                
                                ) 
                                ,
                                'multiple' => true,
                                'required' => true,
                                )
                            )
                        ->add('password', PasswordType::class)
                ;
        }

        public function configureOptions(OptionsResolver $resolver)
        {
                $resolver->setDefaults([
                'data_class' => Utilisateur::class,
                ]);
        }
}
