<?php

namespace App\DataFixtures;

use App\Entity\Demand;
use App\Entity\Utilisateur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;




class DemandFixture extends Fixture

{
    protected Security $security;

    protected UserPasswordHasherInterface $passwordHasher;

    public function __construct(Security $security, UserPasswordHasherInterface $passwordHasher) {
        $this->security = $security;
        $this->passwordHasher = $passwordHasher;
    }


    public function load(ObjectManager $manager): void
    {

 
        $utilisateur =  new Utilisateur();

        $utilisateur->setUsername('USER 1');
        $utilisateur->setRoles(["ROLE_ADMIN"]);
        $utilisateur->setPassword(
            $this->passwordHasher->hashPassword($utilisateur, "test"));
        $manager->persist($utilisateur);

        $manager->flush();


        $user = $this->security->getUser();

        $demand = new Demand();
        $demand->setTitle('Demand Test closedd');
        $demand->setUtilisateur($user);
        $demand->setContent("Hello hello helloooo");
        $demand->setCreationDate(\DateTime::createFromFormat('d-m-Y',"26-12-2022"));
        $demand->setPieceJointe('C:\xampp\tmp\phpFD06.tmp');
        $demand->setStatus('closed');
        $manager->persist($demand);

        $manager->flush();
    }
}
