<?php

namespace App\Tests;

use App\Entity\Demand;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DemandRepositoryTest extends KernelTestCase
{
     /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testSearchByStatus()
    {
        $demand = $this->entityManager
            ->getRepository(Demand::class)
            ->findOneBy(['status' => 'closed'])
        ;

        $this->assertSame('closed', $demand->getStatus());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        // doing this is recommended to avoid memory leaks
        $this->entityManager->close();
        $this->entityManager = null;
    }
}
